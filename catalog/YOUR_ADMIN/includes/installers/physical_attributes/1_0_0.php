<?php
// use $configuration_group_id where needed
$array_of_fields_needed = array(
    'products_attributes_weight',
    'products_attributes_weight_prefix',
    'products_attributes_length',
    'products_attributes_length_prefix',
    'products_attributes_width', 
    'products_attributes_width_prefix',
    'products_attributes_height', 
    'products_attributes_height_prefix');
foreach($array_of_fields_needed as $field){
    if (!nmx_check_field(TABLE_PRODUCTS_ATTRIBUTES, $field)) $db->Execute("ALTER TABLE " . TABLE_PRODUCTS_ATTRIBUTES . " ADD ".$field." VARCHAR(11) NULL default NULL;");
}

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
            ('Max Results Per Page', 'PHYSICAL_ATTRIBUTES_MAX', '".MAX_DISPLAY_RESULTS_CATEGORIES."', 'The maximum number of results on a page', " . $configuration_group_id . ", 1, NOW(), NOW(), NULL, '')");


// For Admin Pages

$zc150 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5));
if ($zc150) { // continue Zen Cart 1.5.0
  // delete configuration menu
  $db->Execute("DELETE FROM ".TABLE_ADMIN_PAGES." WHERE page_key = 'configPhysAttr' LIMIT 1;");
  // add configuration menu
  if (!zen_page_key_exists('configPhysAttr')) {
    if ((int)$configuration_group_id > 0) {
      zen_register_admin_page('configPhysAttr',
                              'BOX_CATALOG_PRODUCTS_ATTRIBUTES', 
                              'FILENAME_CONFIGURATION',
                              'gID=' . $configuration_group_id, 
                              'configuration', 
                              'Y',
                              $configuration_group_id);
        
      $messageStack->add('Enabled Products Physical Attributes Admin Tool Configuration Menu.', 'success');
    }
  }
    // delete configuration menu
  $db->Execute("DELETE FROM ".TABLE_ADMIN_PAGES." WHERE page_key = 'catalogPhysAttr' LIMIT 1;");
  // add configuration menu
  if (!zen_page_key_exists('catalogPhysAttr')) {
    if ((int)$configuration_group_id > 0) {
      zen_register_admin_page('catalogPhysAttr',
                              'BOX_CATALOG_PRODUCTS_ATTRIBUTES', 
                              'FILENAME_PRODUCTS_ATTRIBUTES',
                              '', 
                              'catalog', 
                              'Y',
                              $configuration_group_id);
        
      $messageStack->add('Enabled Products Physical Attributes Admin Tool Configuration Menu.', 'success');
    }
  }
}
