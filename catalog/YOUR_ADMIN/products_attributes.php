<?php
/**
 * @package admin
 * @copyright Copyright 2003-2010 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: attributes_controller.php 2 2010-10-17 05:06:08Z numinix $
 */

  require('includes/application_top.php');
  $post_keywords = zen_db_prepare_input($_POST['keywords']);  
  
  switch($_REQUEST['action']) {
    case 'process':
    
    //Clear Numinix Cache if module present
    if(defined('NMX_DISK_CACHE_VERSION')){
	// Run the Cache clearing
	require_once('../' . DIR_WS_CLASSES . 'nmx_disk_cache.php');
	$nmx_disk_cache = new nmx_disk_cache();
	$nmx_disk_cache->clearCache('*');
    }
    
      // update products
      foreach ($_POST['products_id'] as $products_id) {
        $db->Execute("UPDATE " . TABLE_PRODUCTS . "
                      SET products_weight = '" . $_POST['products_weight'][$products_id] . "',
                          products_length = '" . $_POST['products_length'][$products_id] . "',
                          products_width = '" . $_POST['products_width'][$products_id] . "',
                          products_height = '" . $_POST['products_height'][$products_id] . "'
                      WHERE products_id = " . $products_id . " LIMIT 1;"); 
      }
      // update attributes
      foreach ($_POST['products_attributes_id'] as $products_attributes_id) {
        //if ($products_attributes_id == 2056) {
          //die ($_POST['attributes_height_prefix'][$products_attributes_id]);
        //}
        $db->Execute("UPDATE " . TABLE_PRODUCTS_ATTRIBUTES . "
                      SET products_attributes_weight_prefix = '" . $_POST['attributes_weight_prefix'][$products_attributes_id] . "',
                          products_attributes_weight = '" . $_POST['attributes_weight'][$products_attributes_id] . "',
                          products_attributes_length_prefix = '" . $_POST['attributes_length_prefix'][$products_attributes_id] . "',
                          products_attributes_length = '" . $_POST['attributes_length'][$products_attributes_id] . "',
                          products_attributes_width_prefix = '" . $_POST['attributes_width_prefix'][$products_attributes_id] . "',
                          products_attributes_width = '" . $_POST['attributes_width'][$products_attributes_id] . "',
                          products_attributes_height_prefix = '" . $_POST['attributes_height_prefix'][$products_attributes_id] . "',
                          products_attributes_height = '" . $_POST['attributes_height'][$products_attributes_id] . "'
                      WHERE products_attributes_id = " . $products_attributes_id . " LIMIT 1;"); 
      }
      if (isset($_REQUEST['request']) && $_REQUEST['request'] == 'ajax') {
        exit('success');
      } else {
        zen_redirect(zen_href_link(FILENAME_PRODUCTS_ATTRIBUTES));
      }
      break;
    case 'search':
      if (isset($post_keywords) && $post_keywords != '') {
        $where = ' WHERE pd.products_name LIKE "%' . $post_keywords . '%"';
      }
      // no break
    default:
      $live = false;
      $prods = array();
      if(PHYSICAL_ATTRIBUTES_INACTIVE == 'false'){
          if($where == ''){
              $where = " WHERE p.products_status='1' ";
          }
          else{
              $where .= " and p.products_status='1' ";
          }
      }
      $products_query_raw = "SELECT distinct(p.products_id), pd.products_name, p.products_weight, p.products_width, p.products_height, p.products_length
                             FROM " . TABLE_PRODUCTS . " p
                             LEFT JOIN " . TABLE_PRODUCTS_DESCRIPTION . " pd ON (pd.products_id = p.products_id)
                             " . $where . "
                             GROUP BY p.products_id
                             ORDER BY pd.products_name ASC";
      $products_split = new splitPageResults($_GET['page'], PHYSICAL_ATTRIBUTES_MAX, $products_query_raw, $products_query_numrows);
      $products = $db->Execute($products_query_raw); 
      break;
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?> - <?php echo BOX_CATALOG_PRODUCTS_ATTRIBUTES; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/cssjsmenuhover.css" media="all" id="hoverJS">
<script language="javascript" src="includes/menu.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.4.4.min.js"></script>
<script type="text/javascript"><!--//
  jQuery(document).ready(function() {
    jQuery('form[name="products_attributes"]').live('submit', function() {
      // do an ajax post for every form on page
      jQuery('form[name="products_attributes"]').each(function() {
        jQuery.post("<?php echo zen_href_link(FILENAME_PRODUCTS_ATTRIBUTES); ?>", jQuery(this).serialize() + "&action=process&request=ajax" );
      });
      alert('Forms submitted');
      return false;
    });
    // convert oz to lbs, comment this out if using pounds 
    //jQuery('input[name^="attributes_weight"]').live('blur', function() {
      //if ((jQuery(this).val() != '+' && jQuery(this).val() != '-') && jQuery(this).val().length > 0) {
        //var new_value = jQuery(this).val() * 0.0625;
        //jQuery(this).val(new_value);
      //}
    //});
  });
//--></script>
<script language="javascript"><!--
function popupWindow(url) {
  window.open(url,'popupWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=600,height=460,screenX=150,screenY=150,top=150,left=150')
}
//--></script>
<script type="text/javascript">
  <!--
  function init()
  {
    cssjsmenu('navbar');
    if (document.getElementById)
    {
      var kill = document.getElementById('hoverJS');
      kill.disabled = true;
    }
  }
  // -->
</script>
<style type="text/css">
  label{display:block;float:left;width:100px;margin-left: 20px;}
  hr{margin: 20px 0 0;}
  h1,h4{margin-left: 20px;}
  h1 {margin: 20px; float: left;}
  .product{float:left;width:100%;}
  .product .inner {padding: 0 20px;}
  .productFields{}
  .productField{float:left;}
  .attribute {float:left;}
  .attribute .inner {padding: 0;}
  .pagination{margin-top: 20px;}
  .clearBoth{clear: both;}
  .searchContainer{padding: 20px; float: right;}
  .searchContainer [type="submit"] {margin: 0;}
  input[type="text"] {float: left;margin-right: 20px;}
  input[type="submit"] {margin: 20px 20px 0;}
</style>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="init()">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->
<a href="<?php echo zen_href_link(FILENAME_PRODUCTS_ATTRIBUTES); ?>"><h1><?php echo HEADING_TITLE; ?></h1></a>
<div class="searchContainer">
<?php echo zen_draw_form('products_attributes_search', FILENAME_PRODUCTS_ATTRIBUTES, 'action=search', 'post'); ?>
  <input type="text" name="keywords" /><input type="submit" value="Search" />
</form>
</div>
<br class="clearBoth" />
<?php
  while (!$products->EOF) {
    echo zen_draw_form('products_attributes', FILENAME_PRODUCTS_ATTRIBUTES, 'action=process', 'post');
    echo '<div class="product"><div class="inner">';
    echo '<h2>' . $products->fields['products_name'] . '</h2>';
    echo '<h3>'.TEXT_PRODUCTS_ATTRIBUTES_BASE.'</h3>';
    echo '<div class="productFields"><div class="productField">';
    echo '<label>'.TEXT_PRODUCTS_ATTRIBUTES_BASEWEIGHT.'</label>';
    echo '<input type="text" name="products_weight[' . $products->fields['products_id'] . ']" value="' . $products->fields['products_weight'] . '" /><br class="clearBoth" />';
    echo '</div>';
    echo '<div class="productField">';
    echo '<label>'.TEXT_PRODUCTS_ATTRIBUTES_BASELENGTH.'</label>';
    echo '<input type="text" name="products_length[' . $products->fields['products_id'] . ']" value="' . $products->fields['products_length'] . '" /><br class="clearBoth" />';
    echo '</div>';
    echo '<div class="productField">';
    echo '<label>'.TEXT_PRODUCTS_ATTRIBUTES_BASEWIDTH.'</label>';
    echo '<input type="text" name="products_width[' . $products->fields['products_id'] . ']" value="' . $products->fields['products_width'] . '" /><br class="clearBoth" />';
    echo '</div>';
    echo '<div class="productField">';
    echo '<label>'.TEXT_PRODUCTS_ATTRIBUTES_BASEHEIGHT.'</label>';
    echo '<input type="text" name="products_height[' . $products->fields['products_id'] . ']" value="' . $products->fields['products_height'] . '" /><br class="clearBoth" />';
    echo '</div></div><br class="clearBoth" />';
    echo zen_draw_hidden_field('products_id[' . $products->fields['products_id'] . ']', $products->fields['products_id']);
    $products_attributes = $db->Execute("SELECT pa.products_attributes_id, pa.products_attributes_weight, pa.products_attributes_weight_prefix, pa.products_attributes_length, pa.products_attributes_length_prefix, pa.products_attributes_width, pa.products_attributes_width_prefix, pa.products_attributes_height, pa.products_attributes_height_prefix, pov.products_options_values_name 
                                         FROM " . TABLE_PRODUCTS_ATTRIBUTES . " pa
                                         LEFT JOIN " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov ON (pov.products_options_values_id = pa.options_values_id)
                                         WHERE pa.products_id = " . $products->fields['products_id'] . "
                                         AND pa.attributes_display_only = 0
                                         ORDER BY pov.products_options_values_sort_order ASC;");
    if ($products_attributes->RecordCount() > 0) { 
      echo '<h3>'.TEXT_PRODUCTS_ATTRIBUTES_ATTRIBUTES.'</h3>';
      while(!$products_attributes->EOF) {
        echo '<h4>' . $products_attributes->fields['products_options_values_name'] . '</h4>';
        echo zen_draw_hidden_field('products_attributes_id[' . $products_attributes->fields['products_id'] . ']', $products_attributes->fields['products_attributes_id']);
        echo '<div class="productField">';
        echo '  <label>'.TEXT_PRODUCTS_ATTRIBUTES_WEIGHTPREFIX.'</label>';
        echo '  <input type="text" name="attributes_weight_prefix[' . $products_attributes->fields['products_attributes_id'] . ']" value="' . $products_attributes->fields['products_attributes_weight_prefix'] . '" /><br class="clearBoth" />';
        echo '  <label>'.TEXT_PRODUCTS_ATTRIBUTES_WEIGHT.'</label>';
        echo '  <input type="text" name="attributes_weight[' . $products_attributes->fields['products_attributes_id'] . ']" value="' . $products_attributes->fields['products_attributes_weight'] . '" /><br class="clearBoth" />';
        echo '</div>';
        echo '<div class="productField">';
        echo '  <label>'.TEXT_PRODUCTS_ATTRIBUTES_LENGTHPREFIX.'</label>';
        echo '  <input type="text" name="attributes_length_prefix[' . $products_attributes->fields['products_attributes_id'] . ']" value="' . $products_attributes->fields['products_attributes_length_prefix'] . '" /><br class="clearBoth" />';
        echo '  <label>'.TEXT_PRODUCTS_ATTRIBUTES_LENGTH.'</label>';
        echo '  <input type="text" name="attributes_length[' . $products_attributes->fields['products_attributes_id'] . ']" value="' . $products_attributes->fields['products_attributes_length'] . '" /><br class="clearBoth" />';
        echo '</div>';
        echo '<div class="productField">';
        echo '  <label>'.TEXT_PRODUCTS_ATTRIBUTES_WIDTHPREFIX.'</label>';
        echo '  <input type="text" name="attributes_width_prefix[' . $products_attributes->fields['products_attributes_id'] . ']" value="' . $products_attributes->fields['products_attributes_width_prefix'] . '" /><br class="clearBoth" />';
        echo '  <label>'.TEXT_PRODUCTS_ATTRIBUTES_WIDTH.'</label>';
        echo '  <input type="text" name="attributes_width[' . $products_attributes->fields['products_attributes_id'] . ']" value="' . $products_attributes->fields['products_attributes_width'] . '" /><br class="clearBoth" />';
        echo '</div>';
        echo '<div class="productField">';
        echo '  <label>'.TEXT_PRODUCTS_ATTRIBUTES_HEIGHTPREFIX.'</label>';
        echo '  <input type="text" name="attributes_height_prefix[' . $products_attributes->fields['products_attributes_id'] . ']" value="' . $products_attributes->fields['products_attributes_height_prefix'] . '" /><br class="clearBoth" />';
        echo '  <label>'.TEXT_PRODUCTS_ATTRIBUTES_HEIGHT.'</label>';
        echo '  <input type="text" name="attributes_height[' . $products_attributes->fields['products_attributes_id'] . ']" value="' . $products_attributes->fields['products_attributes_height'] . '" /><br class="clearBoth" />';
        echo '</div><br class="clearBoth" />';
        $products_attributes->MoveNext();
      }
    }
    echo '<br class="clearBoth" /><input type="submit" value="Submit" />';
    echo '<hr />';
    echo '</div></div></form>';
    $products->MoveNext();
  }
?>
<?php
// Split Page
if ($products_query_numrows > 0) {
?>
  <div class="smallText pagination" align="center"><?php echo $products_split->display_count($products_query_numrows, PHYSICAL_ATTRIBUTES_MAX, $_GET['page'], TEXT_PRODUCTS_ATTRIBUTES_NUM_PRODUCTS) . '<br>' . $products_split->display_links($products_query_numrows, PHYSICAL_ATTRIBUTES_MAX, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], zen_get_all_get_params(array('page', 'info', 'x', 'y', 'pID')) ); ?></div>
<?php
}
// Split Page
?>
<!-- body_text_eof //-->
<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
